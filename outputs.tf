output "certificate_authority_arn" {
  description = "The ARN of the certificate authority."
  value       = aws_acmpca_certificate_authority.example.arn
}
