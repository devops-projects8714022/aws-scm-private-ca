# AWS SCM Private Certificate Authority Terraform Module

This module creates an AWS Private Certificate Authority using AWS Certificate Manager (ACM) Private CA.

## Usage

```hcl
module "private_ca" {
  source = "./terraform-aws-scm-private-ca"

  common_name           = "example.com"
  organization          = "Example Corp"
  organizational_unit   = "IT"
  locality              = "City"
  state                 = "State"
  country               = "US"
  tags                  = {
    Environment = "production"
  }
  certificate_file       = "path/to/certificate.pem"
  certificate_chain_file = "path/to/certificate_chain.pem"
  policy                 = "policy_json_string"
  permission_principal   = "acm.amazonaws.com"
  permission_actions     = ["IssueCertificate", "GetCertificate", "ListPermissions"]
}

output "certificate_authority_arn" {
  value = module.private_ca.certificate_authority_arn
}
