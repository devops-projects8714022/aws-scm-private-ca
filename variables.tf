variable "common_name" {
  description = "The common name for your CA."
  type        = string
}

variable "organization" {
  description = "The organization name for your CA."
  type        = string
}

variable "organizational_unit" {
  description = "The organizational unit name for your CA."
  type        = string
}

variable "locality" {
  description = "The locality for your CA."
  type        = string
}

variable "state" {
  description = "The state for your CA."
  type        = string
}

variable "country" {
  description = "The country for your CA."
  type        = string
}

variable "tags" {
  description = "Tags to apply to the CA."
  type        = map(string)
  default     = {}
}

variable "certificate_file" {
  description = "Path to the certificate file."
  type        = string
}

variable "certificate_chain_file" {
  description = "Path to the certificate chain file."
  type        = string
}

variable "policy" {
  description = "Policy for the CA."
  type        = string
}

variable "permission_principal" {
  description = "The principal that is given permission to use the CA."
  type        = string
}

variable "permission_actions" {
  description = "The actions the principal is allowed to perform on the CA."
  type        = list(string)
}
