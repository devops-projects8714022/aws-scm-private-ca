resource "aws_acmpca_certificate_authority" "example" {
  type                = "ROOT"
  certificate_authority_configuration {
    key_algorithm    = "RSA_2048"
    signing_algorithm = "SHA256WITHRSA"
    subject {
      common_name = var.common_name
      organization = var.organization
      organizational_unit = var.organizational_unit
      locality = var.locality
      state = var.state
      country = var.country
    }
  }
  tags = var.tags
}

resource "aws_acmpca_certificate_authority_certificate" "example" {
  certificate_authority_arn = aws_acmpca_certificate_authority.example.arn
  certificate                = file(var.certificate_file)
  certificate_chain          = file(var.certificate_chain_file)
}

resource "aws_acmpca_certificate_authority_policy" "example" {
  resource_arn = aws_acmpca_certificate_authority.example.arn
  policy       = var.policy
}

resource "aws_acmpca_permission" "example" {
  certificate_authority_arn = aws_acmpca_certificate_authority.example.arn
  principal                 = var.permission_principal
  actions                   = var.permission_actions
}

output "certificate_authority_arn" {
  value = aws_acmpca_certificate_authority.example.arn
}
